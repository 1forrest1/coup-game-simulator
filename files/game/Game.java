package game;

import players.*;

import java.util.ArrayList;
import java.util.*;

public class Game {
    private ArrayList<PlayerData> smallPlayerList = new ArrayList<PlayerData>();
    private ArrayList<String> deck = new ArrayList<String>();
    private ArrayList<players.Player> playerList;
    private ArrayList<game.Turn> turnLog = new ArrayList<Turn>();
    private int livePlayers; //number of players still in game
    private int activePlayer = 0;
    private Turn activeTurn;
    public Game(ArrayList<players.Player> playerListIn) {
        playerList = playerListIn;
        //adds cards to deck then shuffles
        for (int i = 0; i < 3; i++) {
            deck.add("Duke");
            deck.add("Assassin");
            deck.add("Contessa");
            deck.add("Captain");
            deck.add("Ambassador");
        }
        Collections.shuffle(deck);

        livePlayers = playerList.size();// inits number of palyers alive at start

        for (int i = 0; i < playerList.size(); i++) {
            smallPlayerList.add(new PlayerData(playerList.get(i).getId()));
            playerList.get(i).provideLogIndex(i);
        }


        turnLog.add(new Turn(smallPlayerList, activePlayer));//adds the first turn object to turn log
        //this adds cards to players and turn object
        for (int i = 0; i < playerList.size(); i++) {
            playerList.get(i).addCard(deck.get(0));
            turnLog.get(0).addCard(i, deck.get(0));
            deck.remove(0);
            playerList.get(i).addCard(deck.get(0));
            turnLog.get(0).addCard(i, deck.get(0));
            deck.remove(0);
            playerList.get(i).addTurnLog(turnLog);
            turnLog.get(0).getPlayers().get(i).incrementCoins(2);

        }
        // this while loop is the full game keeps going until live palyers = 1
    }
    public boolean takeTurn() {

        activePlayer++;
        if (activePlayer >= livePlayers) {
            activePlayer = 0;
        }
        activeTurn = turnLog.get(turnLog.size() - 1);
        //gets the play from the player at the active palyer index in playerlist and passes the turn log
        //System.out.println("active player at getPlay = "+activePlayer);
        int play = playerList.get(activePlayer).getPlay(/*turnLog.get(turnLog.size()-1)*/);
        // 0 = coup, 1 = Assassin, 2 = foreign aid, 3 = Duke, 4 = income, 5 = Ambassador, 6 = Captain
        if (play == 0) {
            resolveCoup();
        }
        if (play == 1) {
            resolveAssassin();
        }
        if (play == 2) {
            resolveForeignAid();
        }
        if (play == 3) {
            resolveDuke();
        }
        if (play == 4) {
            resolveIncome();
        }
        if (play == 5) {
            resolveAmbassador();
        }
        if (play == 6) {
            resolveCaptain();
        }

        for (int j = 0; j < playerList.size(); j++) {
            if (playerList.get(j).getHand().size() == 0) {
                playerList.remove(j);
                activeTurn.killPlayer(j);
            }
        }
        livePlayers = playerList.size();
        //livePlayers = 1; //for testing before fully done
        ArrayList<PlayerData> nextTurnSmallPlayerList = new ArrayList<PlayerData>();
        for (int i = 0; i < activeTurn.getPlayers().size(); i++) {
            nextTurnSmallPlayerList.add(new PlayerData(activeTurn.getPlayers().get(i)));
        }
        turnLog.add(new Turn(nextTurnSmallPlayerList, activePlayer));



/*
        for(int i=0; i<playerList.size(); i++){
            System.out.println(turnLog.get(0).getPlayers().get(i).getActiveCards());

        }
        */
        //System.out.println(deck);



        //playerList = playerListIn;
        //Collections.shuffle(playerList);
        if(livePlayers >1){
            return true;
        }
        else{
            System.out.println("game.Game is done");
            return false;
        }

    }



    private void resolveCoup(){
        if(activeTurn.getPlayers().get(activePlayer).getCoins()<7){
            activeTurn.getPlayers().get(activePlayer).decrementCoins(activeTurn.getPlayers().get(activePlayer).getCoins());//remove coins from couper
            int target = playerList.get(activePlayer).getTarget();//get target from couper player object
            activeTurn.addActions(0, target);
            System.out.println("Coup resolved player "+(activePlayer+1)+" did not have enough coins");

        }else {
            activeTurn.getPlayers().get(activePlayer).decrementCoins(7);//remove coins from couper
            int target = playerList.get(activePlayer).getTarget();//get target from couper player object
            activeTurn.addActions(0, target);
            String discardedCard = playerList.get(target).chooseDiscard();//get card targeted player wants to discard
            activeTurn.discardCard(target, discardedCard);
            System.out.println("Coup resolved");
        }

    }
    private void resolveIncome(){
        activeTurn.addActions(1,-1);//if no target put -1
        activeTurn.getPlayers().get(activePlayer).incrementCoins(1);
        System.out.println("Income resolved");
    }

    private void resolveForeignAid(){
        activeTurn.addActions(2,-1);
        int index = activePlayer+1;
        int blocker = -1;
        //this is checking if anyone wants to block
        while(index != activePlayer && blocker >=0 ){
            if(index > livePlayers){
                index = 0;
            }
            if(playerList.get(index).blockForeignAid()){
                blocker = index;
            }
            index++;

        }
        if(blocker == -1){
            activeTurn.getPlayers().get(activePlayer).incrementCoins(2);
        }else{
            activeTurn.addChallenge(blocker, "foreignAid");
            if(playerList.get(activePlayer).challengeCard("Duke")){
                if(challengeCard(activePlayer,blocker,"Duke")) {
                    activeTurn.getPlayers().get(activePlayer).incrementCoins(2);
                }
            }
        }

        System.out.println("Foreign aid resolved");

    }
    private void resolveDuke(){
        activeTurn.addActions(3,-1);
        int challenger = findChallenger(activePlayer,"Duke");
        if(challenger == -1){
            activeTurn.getPlayers().get(activePlayer).incrementCoins(3);
        }else{
            if(!challengeCard(challenger, activePlayer, "Duke")){
                activeTurn.getPlayers().get(activePlayer).incrementCoins(3);

            }

        }
        System.out.println("Duke resolved");

    }


    private void resolveAssassin(){
        activeTurn.getPlayers().get(activePlayer).decrementCoins(1);
        boolean challengeResult = false; // setting up variable for later
        int target = playerList.get(activePlayer).getTarget();//find assassination target
        activeTurn.addActions(1,target);//add assassination action and target to turn log
        int challenger = findChallenger(activePlayer, "Assassin");//find if anyone wants to challenge assassin,players should be able to find the target though turn log
        if(challenger >=0){//if there was a challenge (challenger will be -1 if no one wants to challenge)
            challengeResult = challengeCard(challenger, activePlayer, "Assassin");//this will be false if the active player has the assassin card
        }

        //START ACTIVE PLAYER DID HAVE ASSASSIN/WASN'T CHALLENGED
        if(challengeResult == false){//if they have/had the assassin card then the player who is targeted can claim Contessa
            if(playerList.get(target).getHand().size()>0){//check if defender lost their last card by challenging the assassin
                 if(playerList.get(target).blockWithContessa()){//check if they want to block assassin with contessa
                     activeTurn.addChallenge(target, "Contessa");//add contessa block to turn log under challenge (a little awkward but im not sure if it will cause a problem yet
                     challenger = findChallenger(target, "Contessa");//sigh now this is checking if anyone wants to challenge the block
                     if (challenger > -1) {
                         if (challengeCard(challenger, target, "Contessa")) {//challenge successful - target does not have contessa
                             if(playerList.get(target).getHand().size()>0) {//check if defender lost their last card by being challenged on the Contessa
                                 //challenge successful and target still alive - TARGET LOSES SECOND CARD
                                 activeTurn.getPlayers().get(target).discardCard(playerList.get(target).chooseDiscard());
                             }
                             //target claimed Contessa, was challenged, died from successful challenge - TARGET ALREADY DIED
                         }
                         //target claimed Contessa, was challenged, but had the contessa - TARGET DOESN'T LOSE CARD
                     }
                     //target does claim Contessa, no challenge to Contessa - TARGET DOESNT LOSE CARD

                 }else{//target doesn't want to claim Contessa - TARGET LOSES CARD
                     activeTurn.getPlayers().get(target).discardCard(playerList.get(target).chooseDiscard());
                 }
            }
           //but the target died by challenging - TARGET ALREADY DIED
        }
        //END ACTIVE PLAYER DID HAVE ASSASSIN/WASN'T CHALLENGED
        //ACTIVE PLAYER WAS CHALLENGED AND DIDN'T HAVE ASSASSIN (RESOLVED BY CHALLENGE CARD)

        System.out.println("Assassin resolved");
    }




    private void resolveAmbassador(){
        activeTurn.addActions(5,-1);
        int challenger = findChallenger(activePlayer,"Ambassador");
        if(challenger > -1){
            if(!challengeCard(challenger,activePlayer,"Ambassador")){//challenger was wrong
                successfulAmbassador();
            }

        }else if(challenger ==-1){//Ambassador went unchallenged
            successfulAmbassador();
        }
        System.out.println("Ambassador resolved");

    }
    private void resolveCaptain() {
        //System.out.println("active player at captain get target = "+activePlayer);
        int target = playerList.get(activePlayer).getTarget();
        activeTurn.addActions(6, target);
        Boolean playerHasCard = true;
        int challenger = findChallenger(activePlayer, "Captain");
        if (challenger > -1) {//someone challenged
            playerHasCard = !challengeCard(challenger, activePlayer, "Captain");
            //challengeCard(challenger,activePlayer,"Captain");
        }
        if(playerHasCard){
            switch (playerList.get(playerList.get(activePlayer).getTarget()).respondToCaptain()) {
                case -1://they don't block
                    appropriateCaptainFunds(target, activePlayer);
                    break;
                case 0://they block w/ Captain
                    challenger = findChallenger(target,"Captain");
                    if(challenger >-1){//someone challenges block
                        if(challengeCard(challenger,target,"Captain")){
                            appropriateCaptainFunds(target,activePlayer);
                        }
                    }else if(challenger ==-1){
                        appropriateCaptainFunds(target,activePlayer);

                    }
                    break;
                case 1: //they block w/ Ambassador
                    challenger = findChallenger(target,"Ambassador");
                    if(challenger >-1){
                        if(challengeCard(challenger,target,"Ambassador")){
                            appropriateCaptainFunds(target,activePlayer);
                        }
                    }else if(challenger ==-1){
                        appropriateCaptainFunds(target,activePlayer);

                    }
                    break;

            }
        }
    }

    private void appropriateCaptainFunds(int target, int captainPlayer){//checks if the captain target has enough coin, and takes coins
        int availableFunds = activeTurn.getPlayers().get(target).getCoins();
        if(availableFunds >=2 ){//if they have enough coins to pay for the captain
            activeTurn.getPlayers().get(target).decrementCoins(2);
            activeTurn.getPlayers().get(captainPlayer).incrementCoins(2);
        }else{
            activeTurn.getPlayers().get(target).decrementCoins(availableFunds);
            activeTurn.getPlayers().get(captainPlayer).incrementCoins(availableFunds);

        }

        System.out.println("Captain resolved");
    }


    /*  helper function, to be used in resolveAmbassador() when the active player is not blocked*/
    private void successfulAmbassador(){
        Collections.shuffle(deck);//shuffle the deck first
        ArrayList<String> returnedCards = playerList.get(activePlayer).selectAmbassadorCards(deck.get(0), deck.get(1));
        //returnedCards is of the form {reject1, reject2, select1, MAYBE: select2}
        deck.set(0, returnedCards.get(0));
        deck.set(1, returnedCards.get(1));
        if(returnedCards.size() == 3) { //player started w/ one card
            activeTurn.getPlayers().get(activePlayer).replaceHand(returnedCards.get(2));
        } else { //player started w/ two cards
            activeTurn.getPlayers().get(activePlayer).replaceHand(returnedCards.get(2), returnedCards.get(3));
        }
    }
    private boolean challengeCard(int challenger, int defender, String card){//this returns true if defender has does not have the card
        activeTurn.addChallenge(challenger,card);
        if(playerList.get(defender).hasCard(card)){//the challenged player had the card
            activeTurn.discardCard(challenger,playerList.get(challenger).chooseDiscard());//the challenger has to discard a card
            playerList.get(defender).addCard(deck.get(0));
            deck.remove(0);
            deck.add(card);
            Collections.shuffle(deck);
            return false;
        }else{//if defender does not have the card
            activeTurn.discardCard(defender,playerList.get(defender).chooseDiscard());
            return true;

        }

    }
    private int findChallenger(int player, String card){
        int index = player+1;
        int challenger = -1;
        //this is checking if anyone wants to block
        while(index != player){
            if(index >= livePlayers){
                index = 0;
            }
            if(playerList.get(index).challengeCard(card)){
                return index;
            }
            index++;

        }
        return -1;
    }

    @Override
    public String toString(){
        String s = new String();
        for(int i = 0; i < turnLog.size(); i++){
            s += turnLog.get(i).toString();
            s += '\n';
            s += '\n';
        }
        return s;

    }


    public ArrayList<String> getDeck() {
        return deck;
    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    public ArrayList<Turn> getTurnLog() {
        return turnLog;
    }

    public int getLivePlayers() {
        return livePlayers;
    }

    public int getActivePlayer() {
        return activePlayer;
    }

    public Turn getActiveTurn() {
        return activeTurn;
    }
}
