package game;

import java.util.ArrayList ;

public class PlayerData {
    private ArrayList<String> activeCards = new ArrayList<String>();
    private ArrayList<String> deadCards = new ArrayList<String>();
    private Integer coins =0;
    private String id;

    PlayerData(String id){
        this.id = id;

    }
    public void addCard(String card){
        activeCards.add(card);
    }
    public void discardCard(String card){
        for(int i=0; i<activeCards.size(); i++){
            if (card.equals(activeCards.get(i))) {
                deadCards.add(activeCards.get(i));
                activeCards.remove(i);
                break;

            }
        }
    }
    public void incrementCoins(int amount){
        coins += amount;
    }
    public void decrementCoins(int amount){
        coins -= amount;
    }
    public Integer getCoins(){
        return coins;
    }

    //to be used in ambassador
    public void replaceHand(String card1, String card2) {
       activeCards.set(0, card1);
       activeCards.set(1, card2);
    }

    //to be used in ambassador
    public void replaceHand(String card1) {
        activeCards.set(0, card1);
    }

    public String getId() {
        return id;
    }

    public ArrayList<String> getActiveCards() {
        return activeCards;
    }

    public ArrayList<String> getDeadCards() {
        return deadCards;
    }

    public PlayerData(PlayerData old){

        coins = old.coins;
        id = old.id;

        for(int i=0; i < old.getActiveCards().size(); i++){
            activeCards.add(old.getActiveCards().get(i));
        }
        for(int i=0; i<old.getDeadCards().size(); i++){
            deadCards.add(old.getDeadCards().get(i));
        }
    }

    @Override
    public String toString() {
        return "game.PlayerData{" +
                "activeCards=" + activeCards +
                ", deadCards=" + deadCards +
                ", coins=" + coins +
                ", id='" + id + '\'' +
                '}'+'\n';
    }
}
