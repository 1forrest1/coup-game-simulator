package game;

import java.util.ArrayList;

public class Turn{
    private ArrayList<PlayerData> smallPlayerList = new ArrayList<PlayerData>();
    private ArrayList<PlayerData> deadPlayerList = new ArrayList<PlayerData>();

    private Integer activePlayer;
    private Integer action;
    private Integer target;
    private ArrayList<Integer>challengers = new ArrayList<Integer>();
    private ArrayList<String>challengedAction = new ArrayList<String>();//not really sure if this is useful
    private ArrayList<String>cardsLost = new ArrayList<String>();
    private ArrayList<Integer> playersWhoLostCards = new ArrayList<Integer>();



    Turn(ArrayList<PlayerData> PlayersIn, Integer activePlayer){
        for(int i=0; i<PlayersIn.size();i++){
            smallPlayerList.add(PlayersIn.get(i));
        }
        this.activePlayer = activePlayer;
    }
    public void addActions(int actionIn, int targetIn){
        action = actionIn;
        target = targetIn;
    }
    public void addChallenge(int challengerIn, String challengerCardIn){
        challengers.add(challengerIn);
        challengedAction.add(challengerCardIn);
    }

    public ArrayList<PlayerData> getSmallPlayerList() {
        return smallPlayerList;
    }

    public ArrayList<PlayerData> getDeadPlayerList() {
        return deadPlayerList;
    }

    public ArrayList<Integer> getChallengers() {
        return challengers;
    }

    public ArrayList<String> getChallengedAction() {
        return challengedAction;
    }

    public void discardCard(int playerIn, String card ){
        smallPlayerList.get(playerIn).discardCard(card);
        cardsLost.add(card);
        playersWhoLostCards.add(playerIn);
    }
    public void addCard(int playerIndex, String card){
        smallPlayerList.get(playerIndex).addCard(card);
    }

    public ArrayList<PlayerData> getPlayers() {
        return smallPlayerList;
    }

    public Integer getAction() {
        return action;
    }

    public Integer getTarget() {
        return target;
    }

    public Integer getActivePlayer() {
        return activePlayer;
    }

    public ArrayList<String> getCardsLost() {
        return cardsLost;
    }

    public ArrayList<Integer> getPlayersWhoLostCards() {
        return playersWhoLostCards;
    }
    public void killPlayer(int index){
        deadPlayerList.add(smallPlayerList.get(index));
        smallPlayerList.remove(index);
    }

    @Override
    public String toString() {
        String s = new String();
        for(int i=0; i<smallPlayerList.size(); i++){
            s += smallPlayerList.get(i).toString()+'\n';

        }
        return "game.Turn{" +'\n'+
                "players.Player data List = "+'\n'
                +  s +
                "deadPlayerList = " + deadPlayerList +
                ", action = " + action +
                ", target = " + target +
                ", challengers = " + challengers +
                ", challengedAction = " + challengedAction +
                ", cardsLost = " + cardsLost +
                ", playersWhoLostCards = "  + playersWhoLostCards +
                '}';
    }
}