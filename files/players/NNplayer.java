package players;

import java.util.ArrayList;
public class NNplayer extends Player {
    public NNplayer(String idIn) {
        super(idIn);
    }
/*
network inputs:
    active player 1
    action 1
    challenger 1
    blocker  1
    target 1
    cards in hand 2 up to 4?
    number of cards 1
    coins 1
    number of live players 1
    for each player: (max 6)
        number of cards 6
        coins 6
    revealed cards = up to 11
    total = 35
output:
    target 1
    action 1
    challenge 1
    discard 1
    block 1
    ambassador discards 2
    block foreign aid 1
 */

    @Override
    protected void chooseTarget() {

    }

    @Override
    public ArrayList<String> selectAmbassadorCards(String card1, String card2) {
        return null;
    }

    @Override
    public int respondToCaptain() {
        return 0;
    }

    @Override
    public boolean challengeCard(String card) {
        return false;
    }

    @Override
    public boolean blockWithContessa() {
        return false;
    }

    @Override
    public String chooseDiscard() {
        return null;
    }

    @Override
    public Integer getPlay() {
        return null;
    }


    @Override
    public boolean blockForeignAid() {
        return false;
    }
}
