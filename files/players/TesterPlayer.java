package players;

import java.util.ArrayList;

public class TesterPlayer extends Player {
    private int getPlayNum;
    public TesterPlayer(String idIn, int playNum) {
        super(idIn);
        this.getPlayNum = playNum;

    }

    @Override
    protected void chooseTarget() {

    }

    @Override
    public ArrayList<String> selectAmbassadorCards(String card1, String card2) {
        System.out.println("selectAmbassadorCards");
        return null;
    }

    @Override
    public int respondToCaptain() {
        System.out.println("respondToCaptain");
        return 0;
    }

    @Override
    public boolean challengeCard(String card) {
        System.out.println("challengeCard");

        return false;
    }

    @Override
    public boolean blockWithContessa() {
        System.out.println("blockWithContessa");

        return false;
    }

    @Override
    public String chooseDiscard() {
        System.out.println("chooseDiscard");

        return null;
    }

    @Override
    public Integer getPlay() {
        System.out.println("getPlay no array param");

        return getPlayNum;
    }




    @Override
    public boolean blockForeignAid() {
        System.out.println("blockForeignAid");

        return false;
    }

}
