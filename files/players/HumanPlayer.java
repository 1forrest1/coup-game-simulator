package players;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class HumanPlayer extends Player {
    public HumanPlayer(String idIn) {
        super(idIn);
    }

    @Override
    protected void chooseTarget() {
        System.out.println("Who do you want to target?");
        for(int i = 0; i< getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().size(); i++) {
            if( i != logIndex){
                System.out.println(getActiveTurn().getSmallPlayerList().get(i).getId()+"-> "+i);
            }
        }
        int resposne = -1;
        try  {
            Scanner scr = new Scanner(System.in);


            while(resposne<0 || resposne>= getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().size()){
                resposne  = scr.nextInt();

            }
        } catch (Exception e) {
            e.printStackTrace();


        }
        target = resposne;


    }

    public ArrayList<String> selectAmbassadorCards(String card1, String card2) {
        showGameState();
        System.out.println("You drew "+card1+" and "+card2);
        int index=0;
        ArrayList<String> cards= new ArrayList<String>();

        for(int i = 0; i< getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().size(); i++){
            System.out.println(i+" = "+ getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().get(i));
            cards.add(getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().get(i));
            index++;
        }
        cards.add(card1);
        cards.add(card2);
        System.out.println(index+1+" = "+card1);
        System.out.println(index+2+" = "+card2);
        ArrayList<String> cardsToReturn= new ArrayList<String>();
        int cardToReturn1 =0;
        int cardToReturn2 =0;
        try  {
            InputStreamReader reader = new InputStreamReader(System.in);

            while(cardToReturn1 == cardToReturn2) {
                System.out.println("enter the numbers for the cards you want to return:");
                cardToReturn1 = reader.read();
                cardToReturn2 = reader.read();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cardsToReturn.add(cards.get(cardToReturn1));
        cardsToReturn.add(cards.get(cardToReturn2));
        return cardsToReturn;




    }
    public boolean blockForeignAid() {
        return false;
    }


    public int respondToCaptain() {
        showGameState();
        System.out.println("Enter -1 to do nothing, 0 to block with Captain, and 1 to Block with ambassador:");
        int resposne = -2;
        try  {
            InputStreamReader reader = new InputStreamReader(System.in);

            while(Math.abs(resposne)>1){
                resposne = reader.read();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resposne;
    }


    public boolean challengeCard(String card) {
        showGameState();
        if(getActiveTurn().getTarget() == getActiveTurn().getActivePlayer()){
            System.out.println(getActiveTurn().getChallengers().get(getActiveTurn().getChallengers().size()-1)+" is challenging "+activeTurn.getAction());

        }
        System.out.println("do you want to challenge? (Y/N)");
        String resposne = "";
        try  {
            Scanner scr = new Scanner(System.in);


            while(!resposne.equals("Y") && !resposne.equals("N")){
                resposne = scr.next();
            }
        } catch (Exception e) {
            e.printStackTrace();


        }
        if(resposne.equals("Y")){
            return true;
        }
        return false;
    }


    public boolean blockWithContessa() {
        showGameState();
        System.out.println("Do you want to block with Contessa? (Y/N)");
        String resposne = "";
        try  {
            Scanner scr = new Scanner(System.in);


            while(!resposne.equals("Y") && !resposne.equals("N")){
                resposne = scr.next();
            }
        } catch (Exception e) {
            e.printStackTrace();


        }
        if(resposne.equals("Y")){
            return true;
        }
        return false;

    }


    public String chooseDiscard() {
        showGameState();
        System.out.println("Which card would you prefer losing?");
        if (getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().size() < 2) {
            return getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().get(0);
        }

        System.out.println("0: " + getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().get(0));
        System.out.println("OR");
        System.out.println("1: " + getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().get(1));

        int choice = -1;

        try {
            Scanner keyboard = new Scanner(System.in);
            choice = keyboard.nextInt();
            if (choice > 1 || choice < 0) {
                throw new Exception("bad input");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return getActiveTurn().getSmallPlayerList().get(logIndex).getActiveCards().get(choice);

    }


    public Integer getPlay() {
        showGameState();

        System.out.println("What is your play?");
        System.out.println("Coup -> 0");
        System.out.println("Claim Assassin -> 1");
        System.out.println("Foreign Aid -> 2");
        System.out.println("Claim Duke -> 3");
        System.out.println("Income -> 4");
        System.out.println("Claim Ambassador -> 5");
        System.out.println("Claim Captain -> 6");

        int play = -1;

        try {

            Scanner keyboard = new Scanner(System.in);
            play = keyboard.nextInt();
            if (play > 6 || play < 0){
                System.out.println(play);
                throw new Exception("Not a valid play");
            }
            //the input was bad
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(play == 0 || play == 1 ||play == 6 ){
            chooseTarget();
        }
        return play;
    }



    private void showGameState(){
        System.out.println("turn  = "+turnLog.size());
        game.PlayerData playerData;
        int index =-1;
        for(int i = 0; i< getActiveTurn().getSmallPlayerList().size(); i++){//iterates through Playerdata objects

            if(getActiveTurn().getSmallPlayerList().get(i).getId().equals(this.id)){//checks if its you
                index = i;
        }else{
                playerData = getActiveTurn().getSmallPlayerList().get(i);
                if(playerData.getActiveCards().size() == 2){
                    System.out.println(playerData.getId()+" Has "/*+playerData.getActiveCards().get(0)+" and "+playerData.getActiveCards().get(1)*/+"2 cards in hand, and "+playerData.getCoins()+" coins" );
                }else if(playerData.getActiveCards().size() == 1){
                    System.out.println(playerData.getId()+" Has "/*+playerData.getActiveCards().get(0)*/+"1 card in hand, and "+playerData.getDeadCards().get(0)+" dead cards and "+playerData.getCoins()+" coins" );

                }else if(playerData.getActiveCards().size() == 0){
                    System.out.println(playerData.getId()+" is dead they/them had "+playerData.getDeadCards().get(0)+" and "+playerData.getDeadCards().get(1));
                }


                }
        }
        if(getActiveTurn().getSmallPlayerList().get(index).getActiveCards().size() == 2){
            System.out.println("you have "+ getActiveTurn().getSmallPlayerList().get(index).getActiveCards().get(0)+" and "+ getActiveTurn().getSmallPlayerList().get(index).getActiveCards().get(1)+" in hand");
            System.out.println("you have "+ getActiveTurn().getSmallPlayerList().get(index).getCoins()+" coins");
        }else if(getActiveTurn().getSmallPlayerList().get(index).getActiveCards().size() == 1){
                System.out.println("you have "+ getActiveTurn().getSmallPlayerList().get(index).getActiveCards().get(0)+" in hand");
                System.out.println("you have "+ getActiveTurn().getSmallPlayerList().get(index).getCoins()+" coins");

            }else{
            System.out.println("you are dead and should not be seeing this");
        }
        playerData = getActiveTurn().getSmallPlayerList().get(getActiveTurn().getActivePlayer());
        if(getActiveTurn().getAction() != null) {
            System.out.println("The current play is " + translatePlay(getActiveTurn().getAction()) + " by " + playerData.getId());
            //System.out.println("The current play is " + getActiveTurn().getAction() + " by " + playerData.getId());

            if (getActiveTurn().getAction() == 1 || getActiveTurn().getAction() == 6 || getActiveTurn().getAction() == 0) {
                System.out.println("Targeting index " + getActiveTurn().getTarget());

                System.out.println("Targeting Id " + getActiveTurn().getSmallPlayerList().get(getActiveTurn().getTarget()).getId());

            }
        }

    }
    //translates the int id of a play into english
    // 0 = coup, 1 = Assassin, 2 = foreign aid, 3 = Duke, 4 = income, 5 = Ambassador, 6 = Captain
    public String translatePlay(int play){
        String translation = "not defined";
        switch (play)
        {
            case 0:
                translation = "Coup";
            case 1:
                translation = "Assassin";
            case 2:
                translation = "Foreign Aid";
            case 3:
                translation = "Duke";
            case 4:
                translation = "Income";
            case 5:
                translation = "Ambassador";
            case 6:
                translation = "Captain";

        }

        return translation;
    }

}
