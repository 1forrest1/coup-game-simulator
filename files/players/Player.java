package players;

import java.util.ArrayList;
import game.*;

public abstract class Player {

    protected ArrayList<Turn> turnLog = new ArrayList<>();
    protected int logIndex;
    protected int target;
    protected String id;
    protected ArrayList<String> hand = new ArrayList<String>();
    protected Turn activeTurn;

    public Player(String idIn){
    id = idIn;
}

    public abstract ArrayList<String> selectAmbassadorCards(String card1, String card2);
    public abstract int respondToCaptain();//retruns -1 for no block, 0 for block with captain, and 1 for block with ambassador
    public abstract boolean challengeCard(String card);
    public abstract boolean blockWithContessa();
    public abstract String chooseDiscard();
    public abstract Integer getPlay();
    public abstract boolean blockForeignAid();
    protected abstract void chooseTarget();

    public Turn getActiveTurn(){
        return turnLog.get(turnLog.size()-1);
    }



    public int getTarget(){
        return target;
    }

    public void addCard(String card){
        hand.add(card);
    }

    public String getId() {
        return id;
    }
    

    public ArrayList<String> getHand() {
        return hand;
    }

    public Boolean hasCard(String card){
        if(hand.contains(card)) {
            hand.remove(hand.indexOf(card));
            return true;
        }
        return false;
    }
    public void updateActiveTurn(){
        activeTurn = turnLog.get(turnLog.size());
    }
    public void provideLogIndex(int index){//gives the players the index of their small player in the turn log
        logIndex = index;
    }


    //returns ArrayList of this form: {reject1, reject2, selected1, MAYBE: selected2} depends on hand size
    //puts the selected cards into this object



    public void addTurnLog(ArrayList<Turn> turnLogIn){
        turnLog = turnLogIn;

    }




}

