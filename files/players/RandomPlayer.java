package players;

import java.util.ArrayList;
import java.util.Collections;
import game.*;

public class RandomPlayer extends Player {

    public void addCard(String card){
        hand.add(card);
    }
    public Integer getPlay() {

        int play = (int)(Math.random()*7);
        if(play == 0 || play == 1 ||play == 6 ){
            chooseTarget();
        }
        return play;
    }
    public RandomPlayer(String id){
        super(id);
    }

    public Integer getPlay(ArrayList<Turn> turnLog) {

        return (int)(Math.random()*7);

    }
    
    //Randomly discard one of the cards, return that card
    @Override
    public String chooseDiscard() {
        if(hand.size()>0) {
            int cardIndex = (int) (Math.random() * hand.size());
            String selectedCard = hand.get(cardIndex);
            hand.remove(cardIndex);
            return selectedCard;
        }
        return "Empty hand";
    }
    
    //75% chance of blocking Foreign aid
    @Override
    public boolean blockForeignAid() {
        return !((int)(Math.random()*4) == 0);
    }
    
 
    //10% chance to challenge any card
    @Override
    public boolean challengeCard(String card) {

        return ((int)(Math.random()*10) == 0);

    }
    
    //50% chance to block with contessa
    @Override
    public boolean blockWithContessa() {
        return ((int)(Math.random()*2) == 0);
    }
    
    //random choice 
    @Override
    public ArrayList<String> selectAmbassadorCards(String card1, String card2) {
        ArrayList<String> allCards = new ArrayList<String>();
        for(int i = 0; i < hand.size(); i++){ //add the 1 or 2 cards in hand
           allCards.add(hand.get(i)); 
        }
        //add the 2 cards from the deck
        allCards.add(card1);
        allCards.add(card2);
        //randomize
        Collections.shuffle(allCards);
        return allCards; 
    }
    
    //equal chance of: dont block, block w/ amb, block w/ captain
    @Override
    public int respondToCaptain() {
    
        return ((int)(Math.random()*3) - 1); //-1, 0, or 1
    }

    @Override
    protected void chooseTarget() {
        target = (int)(Math.random()*getActiveTurn().getSmallPlayerList().size());
        if(target == logIndex){
            System.out.println("here "+logIndex+" target = "+target);
            if(target>0){
                target--;
            }else{
                target++;
            }
            System.out.println("here afterwartds "+logIndex+" target = "+target);

        }





    }
}
