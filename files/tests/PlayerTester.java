package tests;

import players.*;
import game.*;
import java.util.ArrayList;

public class PlayerTester {
    /*
    Player prototype;
    private ArrayList<PlayerData> playerDataList = new ArrayList<PlayerData>();
    private ArrayList<Player> playerList;
    private Turn testTrun;
    private ArrayList<Turn> turnLog = new ArrayList<Turn>();


    // private ArrayList<players.Player> uslessplayerList;

    PlayerTester(Player PrototypeToTest,Player prototypeCopy){
        prototype = PrototypeToTest;
        PlayerData prototypeData = new PlayerData(prototype.getId());
        playerDataList.add(new PlayerData(prototypeCopy.getId()));
        playerDataList.add(prototypeData);
        testTrun = new Turn(playerDataList,0);
        turnLog.add(testTrun);
        playerList.add(prototype);
        playerList.add(prototypeCopy);
        prototype.addTurnLog(turnLog);
        prototypeCopy.addCard("Duke");
        testTrun.getPlayers().get(1).addCard("Duke");
        prototype.addCard("Duke");
        prototype.addCard("Contessa");

        //things that need testing
        testGetPlay();
        testSelectAmbassadorCards();
        testRespondToCaptain();//retruns -1 for no block, 0 for block with captain, and 1 for block with ambassador
        testChallengeCard();
        testBlockWithContessa();
        testChooseDiscard();
        testBlockForeignAid();


    }
    boolean testGetPlay(){
        for(int i =0; i<100; i++){
            int action = prototype.getPlay();
            if(action == 0){
                System.out.println("action choice coup without 7 coins");
            }
            if(action <0 || action >6){

                return false;
            }
        }
        return true; }
    boolean testSelectAmbassadorCards(){//working under assumption that prototype has a duke and contessa
        testTrun.addActions(5,-1);
        ArrayList<String> returnedCards = new ArrayList<String>();
        prototype.selectAmbassadorCards("Duke","Duke");


            boolean contessaFound = false;
            for (int i = 0; i < 2; i++) {
                if (returnedCards.get(0).equals("Contessa") && !contessaFound) {
                    contessaFound = true;
                } else if (returnedCards.get(i).equals("Contessa") && contessaFound) {
                    System.out.println("testSelectAmbassadorCards Failed returned contessa Twice");
                    return false;
                } else if (!returnedCards.get(i).equals("Duke") && contessaFound) {
                    System.out.println("testSelectAmbassadorCards Failed returned " + returnedCards.get(i));
                    return false;
                }


            }

        return true;
    }
    boolean testRespondToCaptain(){
        turnLog.remove(0);
        testTrun = new Turn(playerDataList,0);
        turnLog.add(testTrun);
        testTrun.addActions(5,0);
        //replace turn and add appropriate action and target
        int response = -2;
        for(int i=0; i <100; i++) {
         response = prototype.respondToCaptain();
         if(response >=-1 && response <=1){
             return false;
         }
        }
        return true;
        }
    boolean testChallengeCard(){ return false;}//this wold be kinda hard to test, lkke the palyer is wither gonna return  true or false
    boolean testBlockWithContessa(){
        turnLog.remove(0);
        testTrun = new Turn(playerDataList,0);
        turnLog.add(testTrun);
        testTrun.addActions(1,0);

        return false;}
    boolean testChooseDiscard(){return false;}

    boolean testBlockForeignAid(){return false;}



*/

}
